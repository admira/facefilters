# Requirements:

# pip install pillow
# pip install pynput
from PIL import Image
from PIL import ImageTk
from pynput.keyboard import Key, Controller
import cv2, threading, os, time
from threading import Thread
from os import listdir
from os.path import isfile, join

import dlib
from imutils import face_utils, rotate_bound
import numpy
import math
import time

import subprocess as sp

#keyboard.press (Key.ctrl)
#keyboard.press ('c')
#keyboard.release ('c')
#keyboard.release (Key.ctrl)

command = [ 'ffmpeg.exe',
        '-y', # (optional) overwrite output file if it exists
        '-f', 'rawvideo',
        '-vcodec','rawvideo',
        '-s', '1080x1920', # size of one frame
        #'-s', '720x1280', # size of one frame
        '-pix_fmt', 'bgr24',
        '-r', '24', # frames per second
        '-i', '-', # The imput comes from a pipe
        '-an', # Tells FFMPEG not to expect any audio

        '-f', 'mpegts',
		'-codec:v', 'mpeg1video',
        '-b:v', '20000k',
        '-bf', '0',
        'http://127.0.0.1:8081/supersecret'
       ]

lastFaceTime = time.time();
lastFaceStatus = False;
standBy = 4;

keyboard = Controller();

def checkCallToAction ( face ):
    global lastFaceStatus;
    global lastFaceTime;
    now = time.time();
    print (lastFaceTime)
    if face:
        if not lastFaceStatus:
            writeCondition(1);
            lastFaceStatus = True;
        lastFaceTime = now;
        keyboard.press (Key.ctrl)
        keyboard.release (Key.ctrl)
        
    else:
        if lastFaceStatus and now - standBy > lastFaceTime :
                writeCondition (0);
                lastFaceStatus = False;
     
    return True;

def writeCondition ( value ):
    now = time.time();
    #tstamp = Math.floor( now / 1000 );
    file = open("C:\ADmiraPlayer\Admira\conditions\pressure.xml", "w");
    #file.write("To add more lines.");

    file.write('<?xml version="1.0" encoding="UTF-8"?><condition id="' + '31' + '" tstamp="' + str(int(now)) + '" version="1.0" status="ok" ><value>' + str(value) + '</value><info> Service OK</info></condition>');

    file.close()
    
    return True;


def put_sprite(num):
    global SPRITES, BTNS
    SPRITES[num] = (1 - SPRITES[num]) #not actual value
    if SPRITES[num]:
        BTNS[num].config(relief=SUNKEN)
    else:
        BTNS[num].config(relief=RAISED)


def calculate_inclination(point1, point2):
    x1,x2,y1,y2 = point1[0], point2[0], point1[1], point2[1]
    incl = 180/math.pi*math.atan((float(y2-y1))/(x2-x1))
    return incl

def computeInlinePoint(point1, point2, factor ):
    pointF = [0,0]
    pointF [0] = round ( point1[0] + (point2[0]-point1[0])*factor ) 
    pointF [1] = round ( point1[1] + (point2[1]-point1[1])*factor ) 
    return pointF

def computePointsDistance(point1, point2, factor ):
    dist = math.hypot (point2[0]-point1[0], point2[1]-point1[1]) * factor
    return round ( dist ) 

def getROI ( center, width, sprite, angle ):
    #widthOrig, heightOrig, _ = cv2.GetSize(sprite);
    heightOrig, widthOrig, _ = sprite.shape;
    height = heightOrig * width / widthOrig;
    sin = math.sin(math.radians(angle))
    cos = math.cos(math.radians(angle))
    w = width/2
    x = center[0]
    y = center[1]
    p3 = [ int(round(x-w*cos)), int(round(y-w*sin)) ]
    p4 = [ int(round(x+w*cos)), int(round(y+w*sin)) ]
    xt = x+height*sin
    yt = y-height*cos
    p1 = [ int(round(xt-w*cos)), int(round(yt-w*sin)) ]
    p2 = [ int(round(xt+w*cos)), int(round(yt+w*sin)) ]

    x, y, w, h = calculate_boundbox(numpy.asarray([p1, p2, p3, p4]))

    #return (x, y, w, h, numpy.asarray([p1, p2, p3, p4]) )
    return (x, y, w, h )

def getROIDown ( top, width, sprite, angle ):
    #widthOrig, heightOrig, _ = cv2.GetSize(sprite);
    heightOrig, widthOrig, _ = sprite.shape;
    height = heightOrig * width / widthOrig;
    sin = math.sin(math.radians(angle))
    cos = math.cos(math.radians(angle))
    w = width/2
    x = top[0]
    y = top[1]
    p3 = [ int(round(x-w*cos)), int(round(y-w*sin)) ]
    p4 = [ int(round(x+w*cos)), int(round(y+w*sin)) ]
    xt = x-height*sin
    yt = y+height*cos
    p1 = [ int(round(xt-w*cos)), int(round(yt-w*sin)) ]
    p2 = [ int(round(xt+w*cos)), int(round(yt+w*sin)) ]

    x, y, w, h = calculate_boundbox(numpy.asarray([p1, p2, p3, p4]))

    #return (x, y, w, h, numpy.asarray([p1, p2, p3, p4]) )
    return (x, y, w, h )

def calculate_boundbox(list_coordinates):
    x = min(list_coordinates[:,0])
    y = min(list_coordinates[:,1])
    w = max(list_coordinates[:,0]) - x
    h = max(list_coordinates[:,1]) - y
    return (x,y,w,h)

def draw_sprite(frame, sprite, x_offset, y_offset):
    x_offset = round(x_offset)
    y_offset = round (y_offset)
    (h,w) = (sprite.shape[0], sprite.shape[1])
    (imgH,imgW) = (frame.shape[0], frame.shape[1])

    if y_offset < 0: #if sprite gets out of image in the top
        sprite = sprite[abs(y_offset)::,:,:]
        h = sprite.shape[0]
        y_offset = 0

    if  y_offset >= imgH:
        return frame

    if y_offset+h >= imgH: #if sprite gets out of image in the bottom
        sprite = sprite[0:imgH-y_offset,:,:]
        h = sprite.shape[0]
    

    if x_offset < 0: #if sprite gets out of image to the left
        sprite = sprite[:,abs(x_offset)::,:]
        w = sprite.shape[1]
        x_offset = 0

    if  x_offset >= imgW:
        return frame

    if x_offset+w >= imgW: #if sprite gets out of image to the right
        sprite = sprite[:,0:imgW-x_offset,:]
        w = sprite.shape[1]
    
    
    #for each RGB chanel
    for c in range(3):
            #chanel 4 is alpha: 255 is not transpartne, 0 is transparent background
            frame[y_offset:y_offset+h, x_offset:x_offset+w, c] =  \
            sprite[:,:,c] * (sprite[:,:,3]/255.0) +  frame[y_offset:y_offset+h, x_offset:x_offset+w, c] * (1.0 - sprite[:,:,3]/255.0)
    return frame

def cvloop(run_event):
    global panelA
    global SPRITES

    video_capture = cv2.VideoCapture(0) #read from webcam
    
    #video_capture.set(cv2.CAP_PROP_FRAME_WIDTH, 1280);
    #video_capture.set(cv2.CAP_PROP_FRAME_HEIGHT, 720);

    video_capture.set(cv2.CAP_PROP_FRAME_WIDTH, 1920);
    video_capture.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080);

    (x,y,w,h) = (0,0,10,10) #whatever initial values

    #Filters path
    detector = dlib.get_frontal_face_detector()

    #Facial landmarks
    print("[INFO] loading facial landmark predictor...")
    model = "filters/shape_predictor_68_face_landmarks.dat"
    predictor = dlib.shape_predictor(model) # link to model: http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2

    proc = sp.Popen ( command, stdin=sp.PIPE, shell=False )
    ###while run_event.is_set(): #while the thread is active we loop
    faceDetected = False;
    while run_event: #while the thread is active we loop
        ret, image = video_capture.read()

        ##Horizontal
        #image = cv2.flip( image, 1 )
        #image = image [:, 400:880];

        ##Vertical
        image = cv2.transpose ( image)
        #image = cv2.flip( image, -1 )
        #image = image [280:1000, 120:600];

        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        detectionOffset = [80,100];
        detectionDim = [920,800];
        downSampleFactor = 0.45;
        upSampleFactor = 1.0 / downSampleFactor ;
        gray = gray[ detectionOffset[1] : detectionOffset[1]+detectionDim[1], detectionOffset[0] : detectionOffset[0]+detectionDim[0]]
        #gray = gray[0:1000, : ]
        gray = cv2.resize (gray, (0,0), fx=downSampleFactor, fy=downSampleFactor );
        detectionOffsetScaled = [int(round(detectionOffset[0]*downSampleFactor)), int(round(detectionOffset[1]*downSampleFactor))]
        faces = detector(gray, 0)
        faceDetected = False;
        for face in faces: #if there are faces
            faceDetected = True;
            (x,y,w,h) = (face.left(), face.top(), face.width(), face.height())
            # *** Facial Landmarks detection
            shape = predictor(gray, face)
            shape = face_utils.shape_to_np(shape)
            shape = numpy.add(shape, detectionOffsetScaled)
            incl = calculate_inclination(shape[17], shape[26]) #inclination based on eyebrows
            #print "Angle:"
            #print incl
            # condition to see if mouth is open
            #is_mouth_open = (shape[66][1] -shape[62][1]) >= 10 #y coordiantes of landmark points of lips

            if SPRITES[0]:
                leftEarBase = computeInlinePoint ( shape [8], shape [20], 1.30 )
                rightEarBase = computeInlinePoint ( shape [8], shape [23], 1.30 )
                #print "Ears base: "
                #print leftEarBase
                #print rightEarBase

                earWidth = computePointsDistance  ( shape [17], shape [26], 0.65 )
                #print "Ears width: "
                #print earWidth

                x, y, w, h = getROI ( rightEarBase, earWidth, EARS[0], incl+25)
                #print (x, y, w, h)
                #print ROI
                #cv2.rectangle ( image, (x,y), (x+w, y+h), (255,0,0),1)
                #ROIpoints = ROI.reshape((-1,1,2))
                #cv2.polylines ( image, [ROIpoints], True, (0,255,0),2)

                ear = rotate_bound(EARS[0], incl+25)
                #ear2 = cv2.resize(ear, (0,0), fx=0.3, fy=0.3)
                #heightOrig, widthOrig, _ = ear2.shape;
                #image[0:heightOrig,0:widthOrig] = ear2[:,:,0:3]
                heightOrig, widthOrig, _ = ear.shape;
                factor = upSampleFactor*w/widthOrig;
                ear = cv2.resize(ear, (0,0), fx=factor, fy=factor)

                image = draw_sprite(image,ear, upSampleFactor*x, upSampleFactor*y)


                x, y, w, h = getROI ( leftEarBase, earWidth, EARS[1], incl-25)
                #print (x, y, w, h)
                #print ROI
                #cv2.rectangle ( image, (x,y), (x+w, y+h), (255,0,0),1)
                #ROIpoints = ROI.reshape((-1,1,2))
                #cv2.polylines ( image, [ROIpoints], True, (0,255,0),2)


                ear = rotate_bound(EARS[1], incl-25)
                #ear2 = cv2.resize(ear, (0,0), fx=0.3, fy=0.3)
                #heightOrig, widthOrig, _ = ear2.shape;
                #image[0:heightOrig,0:widthOrig] = ear2[:,:,0:3]
                heightOrig, widthOrig, _ = ear.shape;
                factor = upSampleFactor*w/widthOrig;
                ear = cv2.resize(ear, (0,0), fx=factor, fy=factor)

                image = draw_sprite(image,ear,upSampleFactor*x, upSampleFactor*y)

                #roi = image[y:y+h, x:x+w]
                #image[0:h,0:w] = roi
                #heightOrig, widthOrig, _ = EARS[1].shape;
                #ear = cv2.resize(EARS[1], (0,0), fx=0.1, fy=0.1)
                #cv2.imshow ( "ear" , ear )

                #M = cv2.getRotationMatrix2D((0,0), 0, 0.2)
                #M =numpy.asarray([[1,0,0],[0,1,0]]).T
                #roi = cv2.warpAffine ( EARS[1], M,  (w,h))
                #cv2.imshow ( "test" , roi )
                #cv2.waitKey();
                #image[y:y+h, x:x+w] = cv2.warpAffine ( EARS[1], M,  (w,h) )


            if SPRITES[1]:

                x, y, w, h = calculate_boundbox (shape[:])
                #cv2.rectangle ( image, (x,y), (x+w, y+h), (255,0,0),1)
                w2 = int(round(4.2*w))
                x2 = int(round(x+(w-w2)/2))
                #h2 = int(round ( 1.2*h ))
                y2 = int(round ( y + 1.15*h))
                
                #cv2.rectangle ( image, (x2,y2), (x2+w2, y2+h2), (255,255,0),1)
                jacket = JACKET
                heightOrig, widthOrig, _ = jacket.shape;
                factor = upSampleFactor*w2/widthOrig;
                jacket = cv2.resize(jacket, (0,0), fx=factor, fy=factor)

                image = draw_sprite(image,jacket,upSampleFactor*x2, upSampleFactor*y2)


            if SPRITES[2]:

                x, y, w, h = calculate_boundbox (shape[:])
                #cv2.rectangle ( image, (int(upSampleFactor*x),int(upSampleFactor*y)), (int(upSampleFactor*(x+w)), int(upSampleFactor*(y+h))), (255,0,0),3)
                w2 = int(round(5*w))
                x2 = int(round(x+(w-w2)/2))
                #h2 = int(round ( 1.2*h ))
                y2 = int(round ( y - 0.9*h))
                
                #cv2.rectangle ( image, (x2,y2), (x2+w2, y2+h2), (255,255,0),1)
                suit = SUIT
                heightOrig, widthOrig, _ = suit.shape;
                factor = upSampleFactor*w2/widthOrig;
                suit = cv2.resize(suit, (0,0), fx=factor, fy=factor)

                image = draw_sprite(image,suit,upSampleFactor*x2, upSampleFactor*y2)


            if SPRITES[3]:
                w_helmet = computePointsDistance ( shape [36], shape [45], 3.8 )
                center_eyes = computeInlinePoint ( shape [36], shape [45], 0.5 )
                center_base_helmet = computeInlinePoint ( center_eyes, shape [8], 1.4 )

                x, y, w, h = getROI ( center_base_helmet, w_helmet, HELMET, incl)

                helmet = rotate_bound(HELMET, incl) ;
                #helmet = HELMET
                heightOrig, widthOrig, _ = helmet.shape;
                factor = upSampleFactor*w/widthOrig;
                helmet = cv2.resize(helmet, (0,0), fx=factor, fy=factor)

                #x2 = int(round(center_eyes[0]-(w/2)))
                #y2 = int(round ( center_eyes[1] - 0.4*w)) #if w_helmet = h_helmet aprox

                image = draw_sprite(image,helmet,upSampleFactor*x, upSampleFactor*y)
            if SPRITES[4]:
                w_cap = computePointsDistance ( shape [0], shape [16], 1.9 )
                center_face = computeInlinePoint ( shape [36], shape [45], 0.5 )
                center_base_cap = computeInlinePoint ( shape [8], center_face, 1.2 )

                x, y, w, h = getROI ( center_base_cap, w_cap, CAP, incl)

                cap = rotate_bound(CAP, incl) ;
                #helmet = HELMET
                heightOrig, widthOrig, _ = cap.shape;
                factor = upSampleFactor*w/widthOrig;
                cap = cv2.resize(cap, (0,0), fx=factor, fy=factor)

                #x2 = int(round(center_eyes[0]-(w/2)))
                #y2 = int(round ( center_eyes[1] - 0.4*w)) #if w_helmet = h_helmet aprox

                image = draw_sprite(image,cap,upSampleFactor*x, upSampleFactor*y)
            if SPRITES[5]:
                w_beard = computePointsDistance ( shape [48], shape [54], 2.5 )
                center_base_beard = computeInlinePoint ( shape [33], shape [51], -0.3 )
                #center_base_beard = computeInlinePoint ( center_eyes, shape [8], 1.4 )

                x, y, w, h = getROIDown ( center_base_beard, w_beard, BEARD, incl)

                #cv2.rectangle ( image, (x,y), (x+w, y+h), (255,0,0),1)
                #ROIpoints = ROI.reshape((-1,1,2))
                #cv2.polylines ( image, [ROIpoints], True, (0,255,0),2)


                beard = rotate_bound(BEARD, incl) ;
                #helmet = HELMET
                heightOrig, widthOrig, _ = beard.shape;
                factor = upSampleFactor*w/widthOrig;
                beard = cv2.resize(beard, (0,0), fx=factor, fy=factor)

                #x2 = int(round(center_eyes[0]-(w/2)))
                #y2 = int(round ( center_eyes[1] - 0.4*w)) #if w_helmet = h_helmet aprox

                image = draw_sprite(image,beard,upSampleFactor*x, upSampleFactor*y)

            if SPRITES[6]:

                x, y, w, h = calculate_boundbox (shape[:])
                #cv2.rectangle ( image, (x,y), (x+w, y+h), (255,0,0),1)
                w2 = int(round(10*w))
                x2 = int(round( x + ((w-w2)/2) + w/4 ))
                #h2 = int(round ( 1.2*h ))
                y2 = int(round ( y + 3.6*h))
                
                #cv2.rectangle ( image, (x2,y2), (x2+w2, y2+h2), (255,255,0),1)
                skirt = SKIRT
                heightOrig, widthOrig, _ = skirt.shape;
                factor = upSampleFactor*w2/widthOrig;
                skirt = cv2.resize(skirt, (0,0), fx=factor, fy=factor)

                image = draw_sprite(image,skirt,upSampleFactor*x2, upSampleFactor*y2)


            if SPRITES [7]:
                logo = LOGO
                factor = 0.15;
                logo = cv2.resize(logo, (0,0), fx=factor, fy=factor)
                #print "Logo"
                height, width, _ = logo.shape
                #print height, width
                iHeight, iWidth, _ = image.shape
                #print iHeight, iWidth
                image = draw_sprite(image,logo,iWidth-width, 0)
                #cv2.rectangle ( image, (0,0), (0+width, 0+height), (255,0,0),1)
                #cv2.rectangle ( image, (iWidth-width,0), (iWidth-width+width, 0+height), (255,0,0),1)
            #if SPRITES[5]:
            #    apply_sprite(image, "./sprites/orejas_PR.png",w*1.5,x-(w*1.5-w)/2,y, incl)
            #if SPRITES[6]:
            #    apply_sprite(image, "./sprites/camisa_PR.png",w*5,x-w*2,y+w+h*5, 0)
            #if SPRITES[7]:
            #    apply_sprite(image, "./sprites/logo_PR.png",w,x+100,y, incl)
        if OBJECTS [0]:
            obj = OBJECT[0];
            iHeight, iWidth, _ = image.shape
            height, width, _ = obj.shape
            factor = (iWidth*1.0)/width;
            print (factor)
            obj = cv2.resize( obj, (0,0), fx=factor, fy=factor)
            image = draw_sprite(image,obj,0, 0)
        if OBJECTS [1]:
            obj = OBJECT[1];
            iHeight, iWidth, _ = image.shape
            height, width, _ = obj.shape
            factor = (iWidth*1.0)/width;
            obj = cv2.resize(obj, (0,0), fx=factor, fy=factor)
            image = draw_sprite(image,obj, 0, 50)
        if OBJECTS [2]:
            obj = OBJECT[2];
            factor = 0.45;
            obj = cv2.resize(obj, (0,0), fx=factor, fy=factor)
            image = draw_sprite(image,obj, 365, 610)

        #iHeight2, iWidth2, _ = image.shape
        #print iHeight2, iWidth2;
        #image = cv2.resize (image, (0,0), fx=2, fy=2 );

        #iHeight, iWidth, _ = image.shape
        #print iHeight, iWidth;
        proc.stdin.write(image.tostring());
        checkCallToAction ( faceDetected )

    video_capture.release()



# Variable to control which sprite you want to visualize
SPRITES = [ 0, 0, 1, 0, 0, 0, 0, 0 ] #EARS(2), JACKET, SUIT, HELMET, CAP, BEARD, SKIRT, LOGO -> 1 is visible, 0 is not visible
OBJECTS = [ 0, 0, 0 ] #Stars, Planets, Rocket
#BTNS = [btn0, btn1, btn2]
#EARS =[ cv2.imread('./sprites/orejas_PR.png'), cv2.imread('./sprites/orejas_PR-1.png'), cv2.imread('./sprites/orejas_PR-2.png') ]
#EARS =[ 0,0]
#EARS[0] = cv2.imread('./sprites/orejas_PR-right.png',-1)
#EARS[1] = cv2.imread('./sprites/orejas_PR-left.png',-1)
#JACKET = cv2.imread('./sprites/buzo_cuerpo.png',-1)
SUIT = cv2.imread('./sprites/astronauta_traje.png',-1)
#HELMET = cv2.imread('./sprites/buzo_casco.png',-1)
#CAP = cv2.imread('./sprites/JTI_Carnaval_corona.png',-1)
#BEARD = cv2.imread('./sprites/JTI_Carnaval_barba.png',-1)
#SKIRT = cv2.imread('./sprites/JTI_Carnaval_cua.png',-1)
#LOGO = cv2.imread('./sprites/logo_PR.png',-1)
#BACKGROUND = cv2.imread('./sprites/background_hide.png',-1)
#BACKGROUND = Image.open('./sprites/background_full.png').convert("RGBA")
#BJECT =[ cv2.imread('./sprites/stars.png', -1), cv2.imread('./sprites/planets.png', -1), cv2.imread('./sprites/rocket.png',-1) ]




cvloop (True)


